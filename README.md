# Dashboard Team

This project is using java version `17` LTS.

## Members:
#### Jose Manuel Morales Patty
#### Alex Eduardo Paca Meneses
#### Miguel Angel Romero Sandoval
#### Estefania Daine Rodriguez Apata
#### Santiago Quiroga Salazar
#### Mayerli Santander Sejas

***

## Gradlew

## Common commands
For more info please run `gradle help`.

### Build the project

For windows users:
```shell
gradlew.bat clean build
```
For unix users:

```shell
./gradlew clean build
```


### Run Tests

For windows users:
```shell
gradlew.bat test
```
For unix users:

```shell
./gradlew test
```


### Run validations

For windows users:
```shell
gradlew.bat clean check 
```
For unix users:

```shell
./gradlew clean check
```
