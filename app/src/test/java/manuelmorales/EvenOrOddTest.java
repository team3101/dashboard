package manuelmorales;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test of First Kata: Even Or Odd.
 * <a href="https://www.codewars.com/kata/53da3dbb4a5168369a0000fe/java">Link to Kata</a>
 * Sumary:
 * Create a function that takes an integer as an argument and returns
 * "Even" for even numbers or "Odd" for odd numbers.
 *
 * @author Manuel Morales
 */
public class EvenOrOddTest {
  @Test
  public void testEvenOrOdd() {
    assertEquals("Even", EvenOrOdd.even_or_odd(6));
    assertEquals("Odd", EvenOrOdd.even_or_odd(7));
    assertEquals("Even", EvenOrOdd.even_or_odd(0));
    assertEquals("Odd", EvenOrOdd.even_or_odd(-1));
  }
}
