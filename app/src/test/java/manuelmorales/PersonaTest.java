package manuelmorales;

import org.junit.Test;

/**
 * Test of Persona class.
 * Sumary:
 * This class get the attributes of a person and this class can calculate de
 * IMC and if the persona is or not is an adult person.
 *
 * @author Manuel Morales
 */
public class PersonaTest {
  @Test
  public void testFirstPerson() {
    Persona firstPerson = new Persona("Carlos", 17, "17853423A", 'H', 58.3, 1.62);
    if (firstPerson.calcularImc() == null) {
      System.out.println("Faltan datos para calcular el IMC");
    } else if (firstPerson.calcularImc() == -1) {
      System.out.println("La persona esta por debajo de su peso ideal");
    } else if (firstPerson.calcularImc() == 0) {
      System.out.println("La persona esta en su peso ideal");
    } else {
      System.out.println("La persona tiene sobrepeso");
    }
    if (firstPerson.esMayorDeEdad()) {
      System.out.println("La persona es mayor de edad");
    } else {
      System.out.println("La persona no es mayor de edad");
    }
    System.out.println(firstPerson);
  }

  @Test
  public void testSecondPerson() {
    Persona secondPerson = new Persona("Maria", 35, 'M');
    if (secondPerson.calcularImc() == null) {
      System.out.println("Faltan datos para calcular el IMC");
    } else if (secondPerson.calcularImc() == -1) {
      System.out.println("La persona esta por debajo de su peso ideal");
    } else if (secondPerson.calcularImc() == 0) {
      System.out.println("La persona esta en su peso ideal");
    } else {
      System.out.println("La persona tiene sobrepeso");
    }
    if (secondPerson.esMayorDeEdad()) {
      System.out.println("La persona es mayor de edad");
    } else {
      System.out.println("La persona no es mayor de edad");
    }
    System.out.println(secondPerson);
  }

  @Test
  public void testThirdPerson() {
    Persona thirdPerson = new Persona();
    thirdPerson.setNombre("Alejandro");
    thirdPerson.setEdad(9);
    thirdPerson.setSexo('H');
    thirdPerson.setPesoKg(49);
    thirdPerson.setAlturaM(1.42);
    if (thirdPerson.calcularImc() == null) {
      System.out.println("Faltan datos para calcular el IMC");
    } else if (thirdPerson.calcularImc() == -1) {
      System.out.println("La persona esta por debajo de su peso ideal");
    } else if (thirdPerson.calcularImc() == 0) {
      System.out.println("La persona esta en su peso ideal");
    } else {
      System.out.println("La persona tiene sobrepeso");
    }
    if (thirdPerson.esMayorDeEdad()) {
      System.out.println("La persona es mayor de edad");
    } else {
      System.out.println("La persona no es mayor de edad");
    }
    System.out.println(thirdPerson);
  }
}
