package manuelmorales;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test of Cuenta Class.
 * Sumary:
 * This class get the titular and the balance of an account,
 * this class can be added and subtract money in the account.
 *
 * @author Manuel Morales
 */
public class CuentaTest {
  @Test
  public void testFirstCount() {
    Cuenta cuenta = new Cuenta("Alejandro", 45);
    cuenta.stake(20);
    cuenta.stake(-45);
    assertEquals(65, cuenta.getBalance(), 2d);
    cuenta.whithdraw(55);
    assertEquals(10, cuenta.getBalance(), 2d);
    cuenta.whithdraw(40);
    assertEquals(0, cuenta.getBalance(), 2d);
    System.out.println(cuenta);
  }

  @Test
  public void testSecondCount() {
    Cuenta cuenta = new Cuenta("Carlos");
    assertEquals("Carlos", cuenta.getTitular());
    assertEquals(0, cuenta.getBalance(), 2d);
    cuenta.setTitular("Maria");
    assertEquals("Maria", cuenta.getTitular());
    cuenta.setBalance(34);
    assertEquals(34, cuenta.getBalance(), 2d);
    System.out.println(cuenta);
  }
}
