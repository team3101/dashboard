package manuelmorales;

/**
 * First Kata: Even Or Odd.
 * <a href="https://www.codewars.com/kata/53da3dbb4a5168369a0000fe/java">Link to Kata</a>
 * Sumary:
 * Create a function that takes an integer as an argument and returns
 * "Even" for even numbers or "Odd" for odd numbers.
 *
 * @author Manuel Morales
 */
public class EvenOrOdd {
  private EvenOrOdd() {

  }

  public static String even_or_odd(int number) {
    return number % 2 == 0 ? "Even" : "Odd";
  }
}
