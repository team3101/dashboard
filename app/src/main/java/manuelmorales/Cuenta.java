package manuelmorales;

/**
 * Cuenta Class.
 * Sumary:
 * This class get the titular and the balance of an account,
 * this class can be added and subtract money in the account.
 *
 * @author Manuel Morales
 */
public class Cuenta {
  private String titular;
  private double balance;

  public Cuenta(String titular, double balance) {
    this.titular = titular;
    this.balance = balance;
  }

  public Cuenta(String titular) {
    this.titular = titular;
    this.balance = 0.00;
  }


  public String getTitular() {
    return titular;
  }

  public void setTitular(String titular) {
    this.titular = titular;
  }

  public double getBalance() {
    return balance;
  }

  public void setBalance(double balance) {
    this.balance = balance;
  }

  public void stake(double quantity) {
    this.balance = quantity > 0 ? this.balance + quantity : this.balance;
  }

  public void whithdraw(double quantity) {
    this.balance = this.balance - quantity < 0 ? 0 : this.balance - quantity;
  }

  @Override
  public String toString() {
    return ("""
            El titular de la cuenta es: %s,
            El balance de la cuenta es: %s
            """).formatted(this.titular, this.balance);
  }
}
