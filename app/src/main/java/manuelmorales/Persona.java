package manuelmorales;

/**
 * Persona class.
 * Sumary:
 * This class get the attributes of a person and this class can calculate de
 * IMC and if the persona is or not is an adult person.
 *
 * @author Manuel Morales
 */
public class Persona {
  private String nombre;
  private int edad;
  private final String dni;
  private char sexo;
  private double pesoKg;
  private double alturaM;

  /**
   * This is the first Constructor
   * in this constructor we not receive any parameter.
   */
  public Persona() {
    this.nombre = "";
    this.edad = 0;
    this.dni = generarDni();
    this.sexo = 'H';
    this.pesoKg = 0;
    this.alturaM = 0;
  }

  /**
   * This is the first Constructor
   * in this constructor we receive the name, the age and the sex.
   *
   * @param nombre the name of the person.
   * @param edad the age of the person.
   * @param sexo the sex of the person.
   */
  public Persona(String nombre, int edad, char sexo) {
    this.nombre = nombre;
    this.edad = edad;
    this.dni = generarDni();
    this.sexo = comprobarSexo(sexo);
    this.pesoKg = 0;
    this.alturaM = 0;
  }

  /**
   * This is the first Constructor
   * in this constructor we receive all the parameters.
   *
   * @param nombre the name of the person.
   * @param edad the age of the person.
   * @param dni the dni of the person.
   * @param sexo the sex of the person.
   * @param pesoKg the weight of the person.
   * @param alturaM the height of the person.
   */
  public Persona(String nombre, int edad, String dni, char sexo, double pesoKg, double alturaM) {
    this.nombre = nombre;
    this.edad = edad;
    this.dni = dni;
    this.sexo = comprobarSexo(sexo);
    this.pesoKg = pesoKg;
    this.alturaM = alturaM;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public void setEdad(int edad) {
    this.edad = edad;
  }

  public void setSexo(char sexo) {
    this.sexo = sexo;
  }

  public void setPesoKg(double pesoKg) {
    this.pesoKg = pesoKg;
  }

  public void setAlturaM(double alturaM) {
    this.alturaM = alturaM;
  }

  /**
   * This method can calculate de IMC of a person.
   *
   * @return a number that indicate the status of the person.
   */
  public Integer calcularImc() {
    if (this.pesoKg == 0 || this.alturaM == 0) {
      return null;
    }
    double imc = this.pesoKg / (this.alturaM * this.alturaM);
    return imc < 20 ? -1 : imc <= 25 ? 0 : 1;
  }

  public boolean esMayorDeEdad() {
    return this.edad > 17;
  }

  private char comprobarSexo(char sexo) {
    if (sexo == 'H' || sexo == 'M') {
      return sexo;
    } else {
      return 'H';
    }
  }

  private String generarDni() {
    int dni = (int) (10000000 + Math.random() * 90000000);
    char letra = (char) ((dni % 23) + 65);
    if (letra == 73 || letra == 79 || letra == 85) {
      letra += 1;
    }
    return String.valueOf(dni) + letra;
  }

  @Override
  public String toString() {
    return ("""
            La persona se llama: %s,
            Tiene %s años,
            Su DNI es: %s,
            Su sexo es: %s,
            Su peso es: %s y
            Su altura es: %s
            """).formatted(this.nombre, this.edad, this.dni, this.sexo, this.pesoKg, this.alturaM);
  }
}
