package manuelmorales;

import java.util.Arrays;

/**
 * Third Kata: Expressions Matter.
 * <a href="https://www.codewars.com/kata/5ae62fcf252e66d44d00008e">Link to Kata</a>
 * Sumary:
 * Given three integers a, b ,c, return the largest number obtained after
 * inserting the following operators and brackets: +, *, ()
 * In other words , try every combination of a,b,c with [*+()],
 * and return the Maximum Obtained
 *
 * @author Manuel Morales
 */
public class ExpressionsMatter {
  private ExpressionsMatter() {

  }

  public static int expressionsMatter(int a, int b, int c) {
    return Arrays.stream(new int[]
          {a * (b + c), (a + b) * c, a + b * c, a * b * c, a + b + c}).max().getAsInt();
  }
}
