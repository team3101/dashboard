package manuelmorales;

/**
 * Second Kata: Area Or Perimeter.
 * <a href="https://www.codewars.com/kata/5ab6538b379d20ad880000ab">Link to Kata</a>
 * Sumary:
 * You are given the length and width of a 4-sided polygon.
 * The polygon can either be a rectangle or a square.
 * If it is a square, return its area. If it is a rectangle, return its perimeter.
 *
 * @author Manuel Morales
 */
public class AreaOrPerimeter {
  private AreaOrPerimeter() {

  }

  public static int areaOrPerimeter(int l, int w) {
    return l == w ? l * l : (l * 2) + (w * 2);
  }
}
